import gulp from "gulp";
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);
import cleanCSS from 'gulp-clean-css';
import uglifyES from 'gulp-uglify-es';
const uglify = uglifyES.default;
import concat from 'gulp-concat';
import sourcemaps from 'gulp-sourcemaps';
import autoprefixer from 'gulp-autoprefixer';
import imagemin from 'gulp-imagemin';
import htmlmin from 'gulp-htmlmin';
import newer from 'gulp-newer';
import browserSyncPreset from 'browser-sync';
const browserSync = browserSyncPreset.create();
import del from 'del';

const paths = {
    html: {
        src: './*.html',
        dest: './dist/'
    },
    styles: {
        src: ['./src/styles/**/*.sass', './src/styles/**/*.scss'],
        dest: './dist/css/'
    },
    scripts: {
        src: ['./src/scripts/**/*.js'],
        dest: './dist/js/'
    },
    images: {
        src: './src/img/**',
        dest: './dist/img/'
    }
}

export const clean = () => del(['./dist/*', '!./dist/img'])

export const html = () => {
    return gulp.src(paths.html.src)
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest(paths.html.dest))
    .pipe(browserSync.stream());
}

export const styles = () => {
    return gulp.src(paths.styles.src)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
        overrideBrowserslist: ['last 5 versions', 'ie >= 10'],
        cascade: false
    }))
    .pipe(cleanCSS({
        level: 2
    }))
    .pipe(concat('styles.min.css'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(paths.styles.dest))
    .pipe(browserSync.stream());
}

export const scripts = () => {
    return gulp.src(paths.scripts.src)
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(concat('scripts.min.js'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(paths.scripts.dest))
    .pipe(browserSync.stream());
}

export const img = () => {
    return gulp.src(paths.images.src)
    .pipe(newer(paths.images.dest))
    .pipe(imagemin({
        progressive: true
    }))
    .pipe(gulp.dest(paths.images.dest))
}

export const build = gulp.series(clean, html, gulp.parallel(styles, scripts, img))

export const dev = gulp.series(build, () => {
    browserSync.init({
        server: {
            baseDir: "./dist/"
        }
    })
    gulp.watch('./src/**/*').on('change', browserSync.reload)
    gulp.watch('./*.html').on('change', browserSync.reload)
    gulp.watch(paths.html.src, html)
    gulp.watch(paths.styles.src, styles)
    gulp.watch(paths.scripts.src, scripts)
    gulp.watch(paths.images.src, img)
})

export default dev